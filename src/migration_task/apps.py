from django.apps import AppConfig


class MigrationTaskConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'migration_task'
