from django.core.management.base import BaseCommand
import time
from tqdm import tqdm


class Command(BaseCommand):
    help = "Examle of Django management command"

    def add_arguments(self, parser):
        parser.add_argument(
            "--limit",
            help="Limit (100 by default)"
        )

    def handle(self, *args, **options):

        if options["limit"]:
            for i in tqdm(range(int(options["limit"]))):
                time.sleep(0.1)
        else:
            for i in tqdm(range(100)):
                time.sleep(0.1)
