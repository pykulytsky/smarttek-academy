from django.db import migrations, models
import uuid


def create_some_staff(apps, schema_editor):
    SomeModel = apps.get_model('migration_task', 'SomeModel')

    SomeModel.objects.create(
        some_integer_field=123,
        some_char_field="test",
        uuid_field=uuid.uuid4()
    )


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SomeModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('some_integer_field', models.IntegerField(verbose_name="Integer Field")),
                ('some_char_field', models.CharField(max_length=256)),
                ('uuid_field', models.UUIDField(default=uuid.uuid4(), unique=True)),
            ],
        ),
        migrations.RunPython(create_some_staff)
    ]
