from django.db import models
import uuid


class SomeModel(models.Model):
    some_integer_field = models.IntegerField()
    some_char_field = models.CharField(max_length=256)
    uuid_field = models.UUIDField(default=uuid.uuid4(), unique=True)
